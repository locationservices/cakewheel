Cake Wheel
==========

Installation
------------

Install the application dependencies with:

    npm install


To start the application run:

    npm start

It should now be accessible at http://localhost:3000/


Brief
-----

Create an small application to randomly select a Cake recipe and
display at a minimum:

* Name
* Picture
* Link to recipe

Once a recipe has been chosen it should not be displayed again


Further Development
-------------------

* It should be possible to blacklist certain recipes
* A baker should be chosen from a predefined list
** Once a baker has been picked, he should not be picked again
** It should be possible to add / remove bakers from the list


Resources
---------

* https://developer.yummly.com/documentation
* https://github.com/leesus/yummly-api
