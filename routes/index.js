/* GET home page. */
var request = require('request');
var r = request.defaults({
  'proxy':'http://www-cache.reith.bbc.co.uk:80',
  'timeout': 5000
});

function cakePicker(cakes) {
  var cake = cakes[Math.floor(Math.random() * cakes.length)];
  cake.image = cake.smallImageUrls[0];
  cake.link = "http://www.yummly.com/recipe/" + cake.id;
  console.log(cake);
  
  return cake;
};

function buildQueryUrl(q) {
  var yummly = {
    'key' : '447bf3681a03c0ccee6d0bb019b9da21',
    'id'  : 'a6777993'
  };
  return 'http://api.yummly.com/v1/api/recipes?_app_id=' + yummly.id + '&_app_key=' + yummly.key + '&q=' + q;  
};

function getCakes(callback) {
  console.log('getCakes');
  var queryUrl = buildQueryUrl('cake');
  console.log(queryUrl);

  r(queryUrl, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      callback(JSON.parse(body).matches);
    } else {
      console.log('error', arguments);
    }
  });
};


exports.index = function(req, res) {
  getCakes(function(cakes) {
  	var cake = cakePicker(cakes);
  	res.render('index', { title: 'Express', cake: cake });
  });
};
